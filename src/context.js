import React, { Component } from 'react'
import data from './data'

const ItemContext = React.createContext()

class ItemProvider extends Component {

    state = {
        items: [],
        featuredItems: [],
        itemType: [],
        itemBrand: [],
        favoriteItems: [],
        cartItems: [],
        cartOpen: false,
    }

    componentDidMount() {
        let items = this.formatData(data)
        let featuredItems = items.filter(item => item.featured === true)
        this.setState({
            items,
            featuredItems,
            itemType: items,
            itemBrand: items,
        })
    }

    formatData = (details) => {
        let tempItems = details.map(item => {
            let id = item.sys.id
            let images = item.fields.images.map(image => image.fields.file.url)
            let items = { ...item.fields, id, images }
            return items
        })
        return tempItems
    }

    getItem = (slug) => {
        let tempItem = [...this.state.items]
        const item = tempItem.find(item => item.slug === slug)
        return item
    }

    getSimilar = (type, name) => {
        let tempSimilarItem = [...this.state.items]
        const item = tempSimilarItem.filter(item => item.type === type && item.name !== name)
        return item
    }

    getType = (type) => {
        let tempItemType = [...this.state.itemType]
        const item = tempItemType.filter(item => item.type === type)
        return item
    }

    getBrand = (brand) => {
        let tempItemBrand = [...this.state.itemBrand]
        const item = tempItemBrand.filter(item => item.brand === brand)
        return item
    }

    getItemIndex = (id) => {
        const item = this.state.items.find(item => item.id === id)
        return item
    }
    getFavorite = (id) => {
        let tempFavouriteItem = [...this.state.items]
        const index = tempFavouriteItem.indexOf(this.getItemIndex(id))
        const favoriteItem = tempFavouriteItem[index]
        favoriteItem.favorite = true
        this.setState({
            items: [...tempFavouriteItem],
            favoriteItems: [...this.state.favoriteItems, favoriteItem]
        })
    }
    removeItem = (id) => {
        let tempItems = [...this.state.items]
        let tempFavoriteItems = [...this.state.favoriteItems]

        const index = tempItems.indexOf(this.getItemIndex(id))
        const removedItem = tempItems[index]
        removedItem.favorite = false

        tempFavoriteItems = tempFavoriteItems.filter(item => item.id !== id)

        this.setState({
            items: [...tempItems],
            favoriteItems: [...tempFavoriteItems]
        })
    }
    getCart = (id) => {
        let tempCartItems = [...this.state.items]
        const index = tempCartItems.indexOf(this.getItemIndex(id))
        const cartItem = tempCartItems[index]
        cartItem.cart = true
        this.setState({
            items: [...tempCartItems],
            cartItems: [...this.state.cartItems, cartItem]
        })
    }
    openCart = () => {
        this.setState({
            cartOpen: true
        })
    }
    closeCart = () => {
        this.setState({
            cartOpen: false
        })
    }
    deleteCartItem = (id) => {
        let tempItems = [...this.state.items]
        let tempCartItems = [...this.state.cartItems]

        const index = tempItems.indexOf(this.getItemIndex(id))
        const removedItem = tempItems[index]
        removedItem.cart = false

        tempCartItems = tempCartItems.filter(item => item.id !== id)

        this.setState({
            items: [...tempItems],
            cartItems: [...tempCartItems]
        })
    }

    render() {
        return (
            <ItemContext.Provider value={{
                ...this.state,
                getItem: this.getItem,
                getSimilar: this.getSimilar,
                getType: this.getType,
                getBrand: this.getBrand,
                getFavorite: this.getFavorite,
                removeItem: this.removeItem,
                getCart: this.getCart,
                openCart: this.openCart,
                closeCart: this.closeCart,
                deleteCartItem: this.deleteCartItem,
            }}>
                {this.props.children}
            </ItemContext.Provider>
        )
    }
}

const ItemConsumer = ItemContext.Consumer
export { ItemProvider, ItemConsumer, ItemContext }