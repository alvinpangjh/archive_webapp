import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './pages/Home'
import CategoryType from './components/category/CategoryType'
import SingleItem from './components/singleitem/SingleItem'
import NavBar from './components/navbar/NavBar'
import Brands from './pages/Brands'
import BrandPage from './components/brands/BrandPage'
import FavoritesPage from './pages/Favorites'
import AboutPage from './pages/About'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import Modal from './components/modal/Modal'


export default function App() {
  return (
    <React.Fragment>
      <NavBar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/brands" component={Brands}/>
        <Route exact path="/brands/:brand" component={BrandPage}/>
        <Route exact path="/category/:type" component={CategoryType} />
        <Route exact path="/category/:type/:slug" component={SingleItem} />
        <Route exact path="/favorites" component={FavoritesPage} />
        <Route exact path="/about" component={AboutPage}/>
      </Switch>
      <Modal/>
    </React.Fragment>
  )
}