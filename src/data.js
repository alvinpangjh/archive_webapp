import accessory1 from "./images/data/a1.jpg"
import accessory2 from './images/data/a2.jpg'
import accessory3 from './images/data/a2-1.jpg'
import accessory4 from './images/data/a2-2.jpg'
import accessory5 from './images/data/a3-1.jpg'
import accessory6 from './images/data/a3-2.jpg'
import accessory7 from './images/data/a4-1.jpg'
import accessory8 from './images/data/a4-2.jpg'
import accessory9 from './images/data/a4-3.jpg'
import tee1 from './images/data/t1.jpg'
import tee2 from './images/data/t2.jpg'
import tee3 from './images/data/t3.jpg'
import tee4 from './images/data/t4.jpg'
import tee5 from './images/data/t2-1.jpg'
import tee6 from './images/data/t2-2.jpg'
import tee7 from './images/data/t6-1.jpg'
import tee8 from './images/data/t6-2.jpg'
import tee9 from './images/data/t4-1.jpg'
import tee10 from './images/data/t5-1.jpg'
import tee11 from './images/data/t5-2.jpg'
import cardigan1 from './images/data/h1.jpg'
import cardigan2 from './images/data/h2.jpg'
import cardigan3 from './images/data/h3.jpg'
import cardigan4 from './images/data/h4.jpg'
import jean1 from './images/data/j1.jpg'
import jean2 from './images/data/j2.jpg'
import jean3 from './images/data/j3.jpg'
import sneaker1 from './images/data/s1.jpg'
import sneaker2 from './images/data/s2.jpg'
import sneaker3 from './images/data/s3.jpg'
import sneaker4 from './images/data/s4.jpg'
import boot1 from './images/data/b1.jpg'
import boot2 from './images/data/b2.jpg'
import boot3 from './images/data/b3.jpg'
import boot4 from './images/data/b4.jpg'

export default [
    {
        sys: {
            id: "1"
        },
        fields: {
            name: "Safety pin chain",
            slug: "safety_pin_chain",
            brand: "vintage",
            type: "accessory",
            price: 10,
            size: "free size",
            featured: true,
            favorite: false,
            cart: false,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            images: [
                {
                    fields: {
                        file: {
                            url: accessory1
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: accessory2
                        }
                    }
                }
            ]
        }
    },
    {
        sys: {
            id: "2"
        },
        fields: {
            name: "Metallica Tee",
            slug: "metallica_tee",
            brand: "vintage",
            type: "top",
            price: 250,
            size: "XL",
            featured: true,
            favorite: false,
            cart: false,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ,
            images: [
                {
                    fields: {
                        file: {
                            url: tee1
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: tee2
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: tee3
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: tee4
                        }
                    }
                }
            ]
        }
    },
    {
        sys: {
            id: "3"
        },
        fields: {
            name: "RL Cardigan",
            slug: "rl_cardigan",
            brand: "ralphlauren",
            type: "outerwear",
            price: 20,
            size: "M",
            featured: true,
            favorite: false,
            cart: false,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ,
            images: [
                {
                    fields: {
                        file: {
                            url: cardigan1
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: cardigan2
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: cardigan3
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: cardigan4
                        }
                    }
                }
            ]
        }
    },
    {
        sys: {
            id: "4"
        },
        fields: {
            name: "Versace Jeans",
            slug: "versace_jeans",
            brand: "versace",
            type: "bottom",
            price: 120,
            size: "28",
            featured: true,
            favorite: false,
            cart: false,
            description: "Vintage Versace flared jeans.",
            images: [
                {
                    fields: {
                        file: {
                            url: jean1
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: jean2
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: jean3
                        }
                    }
                },
            ]
        }
    },
    {
        sys: {
            id: "5"
        },
        fields: {
            name: "Converse HiTop",
            slug: "converse_hitop",
            brand: "converse",
            type: "sneaker",
            price: 50,
            size: "US7",
            featured: true,
            favorite: false,
            cart: false,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ,
            images: [
                {
                    fields: {
                        file: {
                            url: sneaker1
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: sneaker2
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: sneaker3
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: sneaker4
                        }
                    }
                }
            ]
        }
    },
    {
        sys: {
            id: "6"
        },
        fields: {
            name: "Dr Martens",
            slug: "dr_martens",
            brand: "drmartens",
            type: "boot",
            price: 180,
            size: "UK7",
            featured: true,
            favorite: false,
            cart: false,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ,
            images: [
                {
                    fields: {
                        file: {
                            url: boot1
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: boot2
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: boot3
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: boot4
                        }
                    }
                }
            ]
        }
    },
    {
        sys: {
            id: "7"
        },
        fields: {
            name: "H&M Gold Circular Necklace",
            slug: "gold_circular_necklace",
            brand: "vintage",
            type: "accessory",
            price: 5,
            size: "free size",
            featured: false,
            favorite: false,
            cart: false,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ,
            images: [
                {
                    fields: {
                        file: {
                            url: accessory3
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: accessory4
                        }
                    }
                },
            ]
        }
    },
    {
        sys: {
            id: "8"
        },
        fields: {
            name: "Gold C Earrings",
            slug: "gold_c_earrings",
            brand: "vintage",
            type: "accessory",
            price: 3,
            size: "free size",
            featured: false,
            favorite: false,
            cart: false,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ,
            images: [
                {
                    fields: {
                        file: {
                            url: accessory5
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: accessory6
                        }
                    }
                },
            ]
        }
    },
    {
        sys: {
            id: "9"
        },
        fields: {
            name: "Mango metal studded bracelet",
            slug: "studded_bracelet",
            brand: "vintage",
            type: "accessory",
            price: 3,
            size: "free size",
            featured: false,
            favorite: false,
            cart: false,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ,
            images: [
                {
                    fields: {
                        file: {
                            url: accessory7
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: accessory8
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: accessory9
                        }
                    }
                },
            ]
        }
    },
    {
        sys: {
            id: "10"
        },
        fields: {
            name: "FUBU Sports Tee",
            slug: "vintage_fubu_sports_tee",
            brand: "vintage",
            type: "top",
            price: 30,
            size: "XL",
            featured: false,
            favorite: false,
            cart: false,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ,
            images: [
                {
                    fields: {
                        file: {
                            url: tee5
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: tee6
                        }
                    }
                },
            ]
        }
    },
    {
        sys: {
            id: "11"
        },
        fields: {
            name: "Calvin Kelin Tee",
            slug: "calvin_kelin_tee",
            brand: "vintage",
            type: "top",
            price: 30,
            size: "S",
            featured: false,
            favorite: false,
            cart: false,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ,
            images: [
                {
                    fields: {
                        file: {
                            url: tee7
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: tee8
                        }
                    }
                },
            ]
        }
    },
    {
        sys: {
            id: "12"
        },
        fields: {
            name: "Kiko Mizuhara Portrait Tee",
            slug: "kiki_mizuhara_tee",
            brand: "vintage",
            type: "top",
            price: 50,
            size: "M",
            featured: false,
            favorite: false,
            cart: false,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ,
            images: [
                {
                    fields: {
                        file: {
                            url: tee9
                        }
                    }
                },
            ]
        }
    },
    {
        sys: {
            id: "13"
        },
        fields: {
            name: "Harley Davidson Tee",
            slug: "harley_davidson_tee",
            brand: "vintage",
            type: "top",
            price: 40,
            size: "XXL",
            featured: false,
            favorite: false,
            cart: false,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ,
            images: [
                {
                    fields: {
                        file: {
                            url: tee10
                        }
                    }
                },
                {
                    fields: {
                        file: {
                            url: tee11
                        }
                    }
                },
            ]
        }
    },
]