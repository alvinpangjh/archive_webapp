import React from 'react'
import Carousel from 'react-bootstrap/Carousel'

import './Carousel.css'
const ImgCarousel = () => {
    return (
        <div className="imgContainer">
            <Carousel interval={3000} indicators={false} controls={false}>
                <Carousel.Item>
                    <img src="images/carousel/carousel1.jpg" alt="carhatt" />
                </Carousel.Item>
                <Carousel.Item>
                    <img src="images/carousel/carousel2.jpg" alt="levis" />
                </Carousel.Item>
                <Carousel.Item>
                    <img src="images/carousel/carousel5.png" alt="adidas" />
                </Carousel.Item>
                <Carousel.Item>
                    <img src="images/carousel/carousel4.png" alt="converse" />
                </Carousel.Item>
            </Carousel>
        </div>

    )
}

export default ImgCarousel