import React, { Component } from 'react'
import { ItemContext } from '../../context'
import { Link } from 'react-router-dom'
import './Category.css'
import EachCategory from './EachCategory'

export default class Category extends Component {

    constructor(props) {
        super(props)
        this.state = {
            type: this.props.match.params.type
        }
    }

    static contextType = ItemContext
    render() {
        const { getType } = this.context
        const items = getType(this.state.type)
        if (!items) {
            return (
                <div className="cat-error">
                    <h1>No such product can be found </h1>
                    <Link to="/" className="cat-errorlink">
                        return home
                    </Link>
                </div>
            )
        }
        const item = items.map(item => <EachCategory key={item.id} item={item} />)
        return (
            <div className="cat-container">
                <h1 className="cat-title">{this.state.type} page</h1>
                <div className="cat-display">
                    {item}
                </div>
            </div>
        )
    }

}