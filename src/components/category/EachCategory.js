import React from 'react'
import Card from 'react-bootstrap/Card'
import { Link } from 'react-router-dom'
import './Category.css'

export default function EachCategory(props) {
    const { name, price, type, slug, images } = props.item;
    return (
            <Card className="cat-card">
                <Link to={`/category/${type}/${slug}`}>
                    <Card.Img src={images[0]} className="imageContainer" />
                </Link>
                <Card.Body>
                    <h6 className="cat-cardtitle">{name}</h6>
                    <div className="cat-cardsubtitle">${price}</div>
                </Card.Body>
            </Card>
    )
}