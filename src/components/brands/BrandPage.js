import React, { Component } from 'react'
import { ItemContext } from '../../context'
import EachBrand from './EachBrand'
import './Brands.css'

export default class BrandPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            brand: this.props.match.params.brand
        }
    }

    static contextType = ItemContext

    render() {
        const { getBrand } = this.context
        let items = getBrand(this.state.brand)
        const item = items.map(item => <EachBrand key={item.id} item={item} context={this.context} />)
        return (
            <div className="brand-container">
                <h1>{this.state.brand}</h1>
                <hr style={{ width: "80%", margin: "auto" }} />
                <div className="brand-display">
                    {item}
                </div>
            </div>
        )
    }
}