import React from 'react'
import Card from 'react-bootstrap/Card'
import { Link } from 'react-router-dom'
import { FaRegHeart } from 'react-icons/fa'

export default function EachBrand(props) {
    const { images, name, price, slug, type, id, favorite } = props.item
    const { getFavorite } = props.context
    return (
        <Card className="brand-card">
            <Link to={`/category/${type}/${slug}`}>
                <Card.Img src={images[0]} alt="" className="brand-image" />
            </Link>
            <Card.Body className="brand-card-body">
                <h6>{name}</h6>
                <div className="brand-pricing">${price}</div>
                <button className="icon-container" disabled={favorite ? true : false} onClick={() => getFavorite(id)}>
                    {favorite ? (<FaRegHeart className="icon-disabled" />) : (<FaRegHeart className="icon-active" />)}
                </button>
            </Card.Body>
        </Card>
    )
}