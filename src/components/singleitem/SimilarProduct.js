import React from 'react'
import Card from 'react-bootstrap/Card'
import { Link } from 'react-router-dom'
import './SingleItem.css'

export default function SimilarProduct(props) {
    const { images, name, price, slug, type } = props.item
    return (
        <Card className="similar-item">
            <Link to={`/category/${type}/${slug}`}>
                <Card.Img src={images[0]} alt="" className="similar-image" />
            </Link>
            <Card.Body className="similar-body">
                <h6>{name}</h6>
                <div style={{ color: "red" }}>${price}</div>
            </Card.Body>
        </Card>
    )
}