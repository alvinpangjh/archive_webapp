import React, { Component } from 'react'
import './SingleItem.css'
import { ItemContext } from '../../context'
import { Link } from 'react-router-dom'
import SimilarProduct from './SimilarProduct'

//create a function that toggles between images when clicked
export default class SingleItem extends Component {

    constructor(props) {
        super(props)
        this.state = {
            slug: this.props.match.params.slug,
            index: 0,
        }
    }

    //used for updating state when slug is changed when similar product is clicked
    componentDidUpdate(prevProps) {
        const { match } = this.props
        const oldSlug = prevProps.match.params.slug
        const newSlug = match.params.slug
        if (newSlug !== oldSlug) {
            this.setState({
                slug: newSlug
            })
        }
    }

    static contextType = ItemContext

    toggleImage = (index) => {
        if (this.state.index !== index) {
            this.setState({
                index: index
            })
        }
    }

    render() {
        const { getItem, getSimilar, removeItem, getFavorite, getCart } = this.context
        const item = getItem(this.state.slug)
        if (!item) {
            return (
                <div className="error-container">
                    <h1>No such item can be found..</h1>
                    <Link to="/" className="error-link">
                        return home
                    </Link>
                </div>
            )
        }

        const { images, name, size, price, type, description, favorite, id, cart } = item

        // enable toggling of images when clicked
        const imageContent = images.map((img, index) => <img src={img} key={index} alt="" onClick={() => this.toggleImage(index)} />)
        const displayImg = images[this.state.index] //display the image which is selected

        const similarItems = getSimilar(type, name)
        const similarItem = similarItems.map(item => <SimilarProduct key={item.id} item={item} />)
        //const similarItem = similarItems.map(item => <FeaturedItem key={item.id} item={item}/>)
        return (
            <React.Fragment>
                <h1 className="product-title">Product</h1>
                <section className="singleitem-container">
                    <div className="image-content">
                        {imageContent}
                    </div>
                    <div className="main-image">
                        <img src={displayImg} alt="" />
                    </div>

                    <div className="image-description">
                        <h2>{name}</h2>
                        <h5>{type}</h5>
                        <div className="sizing"><strong>size:</strong> <span>{size}</span></div>
                        <div className="sizing"><strong>price:</strong> <span>${price}</span></div>
                        <h5 style={{ textTransform: "capitalize", textAlign: "left", margin: "4rem 20px 20px 20px" }}>description:</h5>
                        <h6>{description}</h6>
                        <div style={{ display: "flex", justifyContent: "space-around", alignItems: "center", width: "30%", margin: "auto" }}>
                            {favorite ?
                                <button className="unsave-button" disabled={!favorite ? true : false} onClick={() => removeItem(id)}>
                                    UNSAVE
                                    </button>
                                :
                                <button className="save-button" disabled={favorite ? true : false} onClick={() => getFavorite(id)}>
                                    SAVE
                                    </button>}
                            <button className="add-button" disabled={cart ? true : false} onClick={() => getCart(id)}>
                                {!cart ? <div>ADD TO CART</div> : <div style={{color:"black"}}>ADDED</div> }
                            </button>
                        </div>
                    </div>
                </section>
                <section className="similar-container">
                    {similarItems.length > 1 ?
                        <div>
                            <h1>similar products</h1>
                            <div className="similar-dis">
                                {similarItem}
                            </div>
                        </div> : null}
                </section>
            </React.Fragment>
        )
    }
}