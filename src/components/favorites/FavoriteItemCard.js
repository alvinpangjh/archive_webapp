import React, { Component } from 'react'
import Card from 'react-bootstrap/Card'
import { Link } from 'react-router-dom'
import { FaTrashAlt } from 'react-icons/fa'

export default class FavoriteItemCard extends Component {

    render() {
        const { images, name, price, type, slug, id } = this.props.item
        const { removeItem } = this.props.context
        return (
            <Card className="favorite-card">
                <Link to={`/category/${type}/${slug}`}>
                    <Card.Img src={images[0]} alt="" />
                </Link>
                <Card.Body className="favorite-card-body">
                    <h6>{name}</h6>
                    <p style={{ color: "red" }}>${price}</p>
                    <FaTrashAlt className="fav-icon-container" onClick={() => removeItem(id)} />
                </Card.Body>
            </Card>
        )
    }
}