import React from 'react'
import CartList from './CartList'

export default function ModalCard(props) {
    const { cartItems } = props.context
    let cartList = cartItems.map(item => <CartList key={item.id} item={item} context={props.context} />)

    if (cartItems.length < 1) {
        return (
            <div className="modal-card">
                <div className="modal-card-title">
                    <h4 >CART EMPTY</h4>
                    <hr />
                </div>
            </div>
        )
    }
    return (
        <div className="modal-card">
            <div className="modal-card-title">
                <h4 >CART</h4>
                <hr />
            </div>
            {cartList}
        </div>
    )
}