import React, { Component } from 'react'
import { ItemContext } from '../../context'
import ModalCard from './ModalCard'
import Backdrop from '../backdrop/Backdrop'
import './Modal.css'

export default class Modal extends Component {
    static contextType = ItemContext
    render() {
        const { cartOpen, closeCart } = this.context
        return (
            <div>
                <Backdrop show={cartOpen} clicked={closeCart} />
                <div className="modal-container">
                    {cartOpen ? <ModalCard context={this.context} /> : null}
                </div>
            </div>

        )
    }
}