import React from 'react'
import { FaTrashAlt } from 'react-icons/fa'
import { Link } from 'react-router-dom'

export default function CartList(props) {
    const { images, type, slug, name, id } = props.item
    const { closeCart, deleteCartItem } = props.context
    return (
        <div>
            <div className="modalFlexContainer">
                <Link to={`/category/${type}/${slug}`} onClick={closeCart}>
                    <img src={images[0]} alt="" />
                </Link>
                <p>{name}</p>
                <button type="button" className="modalFlexButton" onClick={() => deleteCartItem(id)}> {/*remove cart item  */}
                    <FaTrashAlt />
                </button>
            </div>
            <hr />
        </div>
    )
}