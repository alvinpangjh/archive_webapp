import React from 'react'
import './Featured.css'
import { Link } from 'react-router-dom'
import Card from 'react-bootstrap/Card'

export default function FeaturedItem(props) {
    const { images, price, name, slug, type } = props.item
    return (
        <Card className="items">
            <Link to={`/category/${type}/${slug}`}>
                <Card.Img src={images[0]} className="img-container" />
            </Link>
            <Card.Body className="text-container">
                <h6 >{name}</h6>
                <p style={{ color: "red" }}>{`$ ${price}`}</p>
            </Card.Body>

        </Card>
        // <article className="items">
        //     <div className="img-container">
        //         <img src={images[0]} alt=""/>
        //     </div>
        // </article>
    )
}


