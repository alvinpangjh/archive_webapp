import React, { Component } from 'react'
import { ItemContext } from '../../context'
import FeaturedItem from '../featured/FeaturedItem'
import './Featured.css'

export default class FeaturedItems extends Component {

    static contextType = ItemContext

    render() {
        let { featuredItems: items } = this.context
        items = items.map(item => {
            return (
                <FeaturedItem key={item.id} item={item} />
            )
        })
        return (
            <section className="featured">
                <div className="featured-title">featured products</div>
                <div className="featured-display">
                    {items}
                </div>
            </section>
        )
    }
}