import React, { Component } from 'react'
import { ItemContext } from '../../context'
import {Link} from 'react-router-dom'
import './Services.css'

//use context.js to provide the props "slug" or "type" from data.js into here so that location can be changed to access the data.js "slug or type" prop.
//eg location in this.state.services can be changed to location: `/category/${slug}`
export default class Services extends Component {
    state = {
        services: [
            {
                id: "1",
                icon: "images/services/necklace.svg",
                title: "Accessories",
                type: "accessory"
            },
            {
                id: "2",
                icon: "images/services/white-t-shirt.svg",
                title: "Tops",
                type: "top"
            },
            {
                id: "3",
                icon: "images/services/hoodie.svg",
                title: "Sweaters",
                type: "outerwear"
            },
            {
                id: "4",
                icon: "images/services/jeans.svg",
                title: "Bottoms",
                type: "bottom"
            },
            {
                id: "5",
                icon: "images/services/sneaker.svg",
                title: "Sneakers",
                type: "sneaker"
            },
            {
                id: "6",
                icon: 'images/services/boot.svg',
                title: "Boots",
                type: "boot"
            }
        ]
    }

    static contextType = ItemContext

    render() {
        return (
            <section className="service">
                <div className="service-title">categories</div>
                <div className="service-container">
                    {this.state.services.map(item => {
                        return (<article key={item.id} item={item}>
                            <Link to={`/category/${item.type}`}>
                                <img src={item.icon} alt="" />
                                <h6>{item.title}</h6>
                            </Link>
                        </article>)
                    })}
                </div>
            </section>
        )
    }
}