import React, { Component } from 'react'
import './NavBar.css'
import { Link } from 'react-router-dom'
import { FaHeart, FaAlignJustify, FaShoppingCart } from "react-icons/fa";
import { ItemContext } from '../../context'

export default class NavBar extends Component {

    state = {
        drawerOpened: false,
        brandOpened: false,
    }

    toggleHandler = () => {
        this.setState({
            drawerOpened: !this.state.drawerOpened
        })
    }
    toggleBrand = () => {
        this.setState({
            brandOpened: !this.state.brandOpened
        })
    }

    static contextType = ItemContext

    render() {
        const { openCart } = this.context
        return (
            <nav className="navigatebar">
                <div className="navigatebar-container">
                    <button className="logo-wrapper" type="button" onClick={this.toggleHandler}>
                        <Link to="/">
                            <FaAlignJustify />
                        </Link>
                    </button>
                    <div className="navigatebar-title">
                        <Link to="/" >
                            Archive
                        </Link>
                    </div>
                    <div>
                        <button className="logo-button" type="button" onClick={() => { }}>
                            <Link to="/favorites">
                                <FaHeart />
                            </Link>
                        </button>
                        <button className="logo-button" onClick={openCart}>
                            <FaShoppingCart />
                        </button>
                    </div>

                </div>
                <div className={this.state.drawerOpened ? "showList listContainer" : "listContainer"}>
                    {/* <Link to="/category">
                        category
                    </Link> */}
                    <Link to="/brands">
                        brands
                    </Link>
                    <Link to="/favorites">
                        favorites
                    </Link>
                    <Link to="/about">
                        about
                    </Link>
                </div>
            </nav>
        )
    }

}