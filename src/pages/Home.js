import React from 'react'
import ImgCarousel from '../components/carousel/ImgCarousel'
import Services from '../components/services/Services'
import FeaturedItems from '../components/featured/Featured'

export default function Home() {
    return (
        <React.Fragment>
            <ImgCarousel />
            <Services />
            <FeaturedItems/>
        </React.Fragment>
    )
}