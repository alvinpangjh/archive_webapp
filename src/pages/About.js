import React from 'react'
import './About.css'

export default function AboutPage() {
    return (
            <div className="about-container">
                <h2>About</h2>
                <hr/>
                <div className="about-desc">
                    <p>Description:</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ornare ullamcorper condimentum. Nam lacinia elementum accumsan. Nunc ultrices consectetur porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque tempor condimentum felis, vel fringilla libero viverra et. Sed ac aliquam nulla. Nunc posuere finibus elit, tempor feugiat libero vulputate quis. Quisque rhoncus aliquam nibh nec facilisis. In vestibulum et ex in rutrum. Quisque mollis ligula ligula, sed tristique tellus mollis sed. Morbi bibendum mi non nunc tincidunt tincidunt hendrerit ut urna. Donec rhoncus, dui eget tempor bibendum, lectus purus suscipit enim, eget mattis eros enim at risus. Phasellus id ex non neque tempor tempus vel nec augue.
                    <br /><br />Ut sit amet elit dictum, hendrerit nisi vel, aliquet odio. Sed vel libero sed ligula accumsan facilisis. Integer velit nisi, lacinia sed euismod in, laoreet in leo. In volutpat tempor turpis. Sed commodo sapien vel dapibus porta. In tristique mauris sit amet mauris ultrices, a mattis velit facilisis. Cras bibendum sapien augue, nec vehicula orci ultricies sed. Interdum et malesuada fames ac ante ipsum primis in faucibus.
                    <br /><br />Nullam at ipsum eu diam hendrerit molestie. Nunc blandit accumsan urna. Nunc semper lacus non magna suscipit, elementum sagittis risus tristique. Sed maximus lacus tortor, nec iaculis nibh maximus at. Suspendisse potenti. Etiam leo turpis, euismod eget tempus id, fringilla vel lacus. Proin id venenatis ipsum. Cras at sem eget metus aliquet ultricies malesuada et odio. Fusce tempor ut est et consequat. Mauris aliquam malesuada nulla, at porta est tristique in. Cras malesuada nec odio sed lobortis. Ut a mattis mauris, in auctor dolor. Vestibulum sit amet orci nec dui gravida auctor. Ut imperdiet vulputate porta. Sed tincidunt justo tincidunt ultricies faucibus.
                </p>
                </div>
            </div>
    )
}