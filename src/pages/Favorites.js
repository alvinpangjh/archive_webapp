import React, { Component } from 'react'
import { ItemContext } from '../context'
import FavoriteItemCard from '../components/favorites/FavoriteItemCard'
import '../components/favorites/Favorites.css'

export default class FavoritesPage extends Component {

    static contextType = ItemContext
    render() {
        const { favoriteItems } = this.context
        if (favoriteItems.length < 1) {
            return (
                <div>
                    <h1 style={{ fontStyle: "italic", textAlign: "center", margin: "5rem auto 2rem auto", textTransform: "capitalize" }}>no item found</h1>
                    <hr style={{ width: "80%", margin: "auto" }} />
                </div>
            )
        }
        let favoriteItem = favoriteItems.map(item => { return (<FavoriteItemCard item={item} key={item.id} context={this.context}/>) })
        return (
            <div className="favorites-container">
                <h1>favorites</h1>
                <hr style={{ width: "80%", margin: "auto" }} />
                <div className="favCard-container">
                    {favoriteItem}
                </div>
            </div>
        )
    }
}