import React, { Component } from 'react'
import '../components/brands/Brands.css'
import {Link} from 'react-router-dom'

export default class Brands extends Component {
    render() {
        return (
            <div className="brands-container">
                <h2>brands</h2>
                <hr/>
                <ul className="list-container">
                    <li>
                        <Link to={`/brands/vintage`}>
                            vintage
                        </Link>
                    </li>
                    <li>
                        <Link to={`/brands/ralphlauren`}>
                            ralph lauren
                        </Link>
                    </li>
                    <li>
                        <Link to={`/brands/drmartens`}>
                            dr. martens
                        </Link>
                    </li>
                    <li>
                        <Link to={`/brands/nike`}>
                            reebok
                        </Link>
                    </li>
                    <li>
                        <Link to={`/brands/converse`}>
                            converse
                        </Link>
                    </li>
                    <li>
                        <Link to={`/brands/nike`}>
                            nike
                        </Link>
                    </li>
                    <li>
                        <Link to={`/brands/adidas`}>
                            adidas
                        </Link>
                    </li>
                    <li>
                        <Link to={`/brands/puma`}>
                            puma
                        </Link>
                    </li>
                    <li>
                        <Link to={`/brands/vans`}>
                            vans
                        </Link>
                    </li>
                    <li>
                        <Link to={`/brands/alyx`}>
                            alyx
                        </Link>
                    </li>
                    <li>
                        <Link to={`/brands/stone-island`}>
                            stone island
                        </Link>
                    </li>
                    <li>
                        <Link to={`/brands/supreme`}>
                            supreme
                        </Link>
                    </li>
                </ul>
                <hr/>
            </div>
        )
    }
}